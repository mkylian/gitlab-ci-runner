#!/bin/sh

GITLABCI_URL=https://git.plzen.eu/
GITLABCI_TOKEN=CBsvx_6pua2siqYUPQkE
RUNNER_NAME=$(hostname)

docker exec -ti gitlab-runner gitlab-ci-multi-runner register \
   --non-interactive \
   --url $GITLABCI_URL \
   --registration-token $GITLABCI_TOKEN \
   --name $RUNNER_NAME \
   --executor docker \
   --docker-image docker:git \
   --docker-volumes '/var/run/docker.sock:/var/run/docker.sock' \
   --docker-volumes '/builds:/builds'

